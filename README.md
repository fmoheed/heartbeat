# Heartbeat

## Netflow Web Interface for Nfdump

Created By: Faadhil Moheed &lt;fm363@cornell.edu&gt;

MEng Mentor: Dan Villanti &lt;dev25@cornell.edu&gt;

### Project Description

The goal of this project was to create a multi-user web interface for analyzing and retrieving network traffic via nfcapd file representations. The interface serves as a wrapper for the [nfdump command line tool](https://github.com/phaag/nfdump) and contains the following features:

* Multi-user authentication and privilege assignment
* Dynamic command generation based on user input
* Fast search capabilities through parallel processing techniques
* Interface to view/export search files from multiple users
* Dashboard to view related charts/metrics

The full code for this project is available on GitHub [here](https://github.coecis.cornell.edu/fm363/Heartbeat). Please email fm363@cornell.edu for trouble accessing the code.

### Packages/Utilities

The web interface is built on a Flask framework. The required packages are detailed in the `requirements.txt` file as well as below:

* Flask
* Flask-Admin
* Flask-SQLAlchemy
* Flask-Security
* Gunicorn

The database environment used for development was SQLite. This can be ported to any MySQL/PostgresSQL instance very easily.

### Usage

* Clone or download the git repository
	```sh
	$ git clone https://github.coecis.cornell.edu/fm363/Heartbeat.git
	```

* Download [Anaconda](https://www.anaconda.com/)

* Create an Anaconda environment and activate
	```sh
	$ conda create --name Heartbeat python=3.6
	$ source activate Heartbeat
	```

* Install dependencies
	```sh
	$ pip install -r requirements.txt
	```

* Run the interface
	```sh
	$ python heartbeat.py
	```

* Open your favorite browser and type:
	```
	http://localhost:5000
	```

### Server Environment

The application needs to be configured on the host server to accept inbound traffic. To do this, we can create a WSGI entry point and bind this to a gunicorn web application server. We can then configure Nginx to direct all traffic to this entry point. The tutorial [here](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-centos-7) provides a high level walkthrough about the steps you should take to configure the server environment. The tutorial is based on a **CentOS7** operating system.

### Configurations

The project configuration files are within the `configs/` directory. The `general_config.py` file details relevant filepaths for various stages in the interface pipeline. Make sure to update these values according to your specific environment. The `application_config.py` file contains global variables central to the application itself. Please make sure to double check before changing these values as you may have to modify parts of the code to reflect the changes.

### Project Features

#### User Roles and Privileges

The interface features a multi-user environment where roles can be created and assigned to specific users that afford certain privileges. Currently there are two roles (User, SuperUser) and two users (one being the Admin account). The Admin credentials are as follows:

`Admin Username: admin`

`Admin Password: admin`

The Admin account can create users, delete users, and edit user records through the interface itself. Regular users can view and interact with the Pulse files (search, view running processes, and the file viewer).

#### Nfdump Search

Users can input various pieces of data through the Pulse Search form and the output generated will be a valid nfdump command. The search form takes into account variability and user defined options. For example the user is able to simply create a custom command from scratch or add custom filtering options that are not defined explicitly in the previous form sections.

#### Subprocess/Parallelism

After the search form is submitted, the generated command is checked for correctness (will return an error if invalid) and if valid, a subprocess is created with the specified command that runs as a background process. The user will be redirected to the Pulse Processes page where they will see a table of the current running processes with records detailing which user started the process. 

The overall process flow is as follows:

* User completes Pulse Search form
* Subprocess is created and executed
* A directory for the user is created in `/log/searches/<current_user>/` (or wherever you specified by updating the `general_config.py` file)
* When the process is finished, the output file is placed in the directory detailed above

A maintenance script (contained within `cron_scripts/maintenance.py`) will poll the `/log/searches/` directory and update the database process table accordingly (changes status field to `COMPLETED` if file appears). In addition the maintenance script removes any searches that are over 2 weeks old (configurable) and removes any user subdirectories that are empty.

#### File View

In order to share searches among users, the Pulse Data page polls the `/log/searches/` directory and displays the current files for each user directory. You can click on a file and the CSV table representation will display on the page (capped at 50 rows, this number can be changed in the `general_config.py` file).

#### Dashboard

Per Dan's request, I did not add too much to the dashboard. The dashboard right now serves as an example of a possible iteration, however everything is currently static content. A milestone moving forward would be to add CRON job scripts to poll data in advance and then update the graphs accordingly. For this, utilizing nfdump's builtin statistics filtering options would be really useful.

### Citations

* Base Styling and Flask Outline - https://github.com/jonalxh/Flask-Admin-Dashboard
* Nfdump Documentation - https://github.com/phaag/nfdump