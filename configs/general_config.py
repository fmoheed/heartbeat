# General Configuration file for app

"""
    Number of available cores for parallel processing
"""
TOTAL_NUM_CORES = 4
USABLE_CORES = TOTAL_NUM_CORES - 1

"""
    Root directory for project
"""
ROOT_DIRECTORY = '/opt/Heartbeat/'

"""
    Path to nfcapd data directories
    
    We should be pointing to the following (example):

    /<directory_path>/node/
        2018-08-08/
            nfacapd.201808082350
            ...
        2018-08-09/
            nfcapd.201808092350
            ...
        ...
        
    Can modify the path to be any of the subdirectories (node, border, etc.)
"""
NFCAPD_DATA_PATH = '/log/flows/node/'

"""
    Path to searches directory
    
    The searches directory has the following format (example):
    
    /<directory_path>/searches/
        user_1/
            search1.csv
            ...
        user_2/
            search2.csv
        
        ...
        
    Every time a user runs a search, the output file is moved into the correct directory for that user
"""
SEARCH_DIRECTORY_PATH = '/log/searches/'

"""
    Path to searches subdirectory

    NOTE: Make sure to place a {} where the user_identifier is supposed to go in the filepath
    
    The subdirectory has the following format (example):
    
    /<directory_path>/searches/<user_identifier>/
        search1.csv
        ...
"""
SEARCH_SUBDIRECTORY_PATH = '/log/searches/{}/'

"""
    Process status flag for PulseProcesses DB
"""
PROCESS_STATUS = {'running': 'RUNNING', 'completed': 'COMPLETED'}

"""
    Base command for nfdump gen_command() function
"""
BASE_COMMAND = ['nfdump']

"""
    Number of rows to display in CSV snapshot
"""
CSV_ROWS = 50
