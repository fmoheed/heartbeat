import os
import csv
import shlex
import re
import subprocess
from configs import general_config, application_config

# All possible format fields
FORMAT_FIELDS = ['Date first seen', 'Date last seen', 'Duration', 'Proto', 'Src IP Addr',
                'Dst IP Addr', 'Src IP Addr:Port', 'Dst IP Addr:Port', 'Src Pt', 'Dst Pt',
                'Src AS', 'Dst AS', 'Input', 'Output', 'Packets', 'Bytes', 'Flows', 'Flags', 'Tos',
                'bps', 'pps', 'Bpp']

# Set test flag
TEST_FLAG = False


# DB AND SEARCH FUNCTIONS
# Check if database already exists in root directory
def db_exists():
    path = os.path.join(general_config.ROOT_DIRECTORY, application_config.DATABASE_FILE)
    return os.path.exists(path)


# Create empty database file from scratch
def create_db(db, app, role, hash_password, data_store):
    db.drop_all()
    db.create_all()

    with app.app_context():
        user_role = role(name='User',
                         description='General user - access to dashboard, Pulse search, and Pulse Processes')
        super_user_role = role(name='SuperUser',
                               description='Admin user - access to dashboard, Pulse search, pulse processes, and User/Role dashboards')
        db.session.add(user_role)
        db.session.add(super_user_role)
        db.session.commit()

        # CREATE ADMIN USER
        data_store.create_user(
            first_name='Admin',
            email='admin',
            roles=[user_role, super_user_role],
            password=hash_password('admin')
        )

        # Fm363
        data_store.create_user(
            first_name='Faadhil',
            email='fm363@cornell.edu',
            roles=[user_role],
            password=hash_password('heartbeat')
        )

        db.session.commit()
    return


# Create the searches directory in /log/searches/ if it does not exist
def create_searches_directory():
    if not os.path.exists(general_config.SEARCH_DIRECTORY_PATH):
        os.mkdir(general_config.SEARCH_DIRECTORY_PATH)
    return


# Create the subdirectory for the current user within /log/searches/<current_user>/ if it does not exist
def create_searches_subdirectory(user):
    if not os.path.exists(general_config.SEARCH_SUBDIRECTORY_PATH.format(user)):
        os.mkdir(general_config.SEARCH_SUBDIRECTORY_PATH.format(user))
    return


# Create search output csv filename (concatenate <current_datetime>_<email_prefix>.csv)
def create_searches_file_name(email_prefix, time_string):
    prefix_string = '_' + email_prefix + '.csv'
    return time_string + prefix_string


# NFDUMP FUNCTIONS
# Generates nfdump command based on form logic from Pulse
def gen_command(form_data, email_prefix, time_string):
    cmd = []

    # Instantiate base command
    cmd += general_config.BASE_COMMAND
    path_to_directory = general_config.NFCAPD_DATA_PATH + form_data['start_directory']

    # Output argument
    name = email_prefix
    path = general_config.SEARCH_SUBDIRECTORY_PATH.format(name)
    output_argument = '> {}'.format(path)

    # If custom command present, then overwrite cmd
    if form_data['custom_command']:
        shlex_output = shlex.split(output_argument, posix=False)
        command_output = shlex.split(form_data['custom_command'], posix=False)

        shlex_output[-1] += create_searches_file_name(email_prefix, time_string)

        return command_output + shlex_output

    # If specific file present, concatenate both directory and filename with -r
    if form_data['nfcapd_file']:
        cmd += ['-r', path_to_directory + '/' + form_data['nfcapd_file']]
    else:
        cmd += ['-R', path_to_directory]

    # If entry_limit, add -c
    if form_data['entry_limit']:
        cmd += ['-c', form_data['entry_limit']]


    # FORMAT OPTIONS
    if form_data.getlist('format_options'):
        format_string = 'fmt:{}'
        accum = ''
        for entry in form_data.getlist('format_options'):
            s = '{},'.format(entry)
            accum = accum + s
    
        format_string = format_string.format(accum[:-1])
    
        cmd += ['-o', "{}".format(format_string)]
    else:
        # Add -o csv
        cmd = cmd + ['-o', 'csv']

    # Check custom filtering
    if form_data['custom_filtering']:
        cmd += ['"{}"'.format(form_data['custom_filtering'])]
    else:
        filter_string = ''
        if form_data['duration']:
            filter_string += 'duration > ' + str(form_data['duration'])

        if form_data['source_ip']:
            filter_string += ' and src ip ' + str(form_data['source_ip'])

        if form_data['destination_ip']:
            filter_string += ' and dst ip ' + str(form_data['destination_ip'])

        if form_data['source_port']:
            filter_string += ' and src port == ' + str(form_data['source_port'])

        if form_data['destination_port']:
            filter_string += ' and dst port == ' + str(form_data['destination_port'])

        if form_data['protocol']:
            filter_string += ' and proto ' + str(form_data['protocol'])

        # Check if filter_string starts with ' and'
        if filter_string.startswith(' and '):
            filter_string = filter_string[5:]

        # Check if filter_string is ''
        if not filter_string == '':
            cmd += ['"{}"'.format(filter_string)]

    # Append output_argument to cmd
    cmd += shlex.split(output_argument, posix=False)

    # Create unique filename
    filename = create_searches_file_name(email_prefix, time_string)
    cmd[-1] += filename

    return cmd


# Checks output of command to make sure you can run
def check_command_ouput(cmd):
    try:
        subprocess.check_output(cmd, shell=True)
    except subprocess.CalledProcessError as e:
        return False, e

    return True, None


# Executes the child process for the nfdump command
def execute_child_process(cmd):
    subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    return


# Updates PulseProcesses DB
def update_pulse_processes(db, PulseProcesses, user_email, process_name, process_command):
    proc = PulseProcesses(user_email=user_email, process_status=general_config.PROCESS_STATUS['running'],
                          process_name=process_name, process_command=process_command)
    db.session.add(proc)
    db.session.commit()
    return


# Retrieves CSV file and parse
def parse_csv(user, filename):
    path = general_config.SEARCH_SUBDIRECTORY_PATH.format(user)
    path += filename

    with open(path, 'r') as f:
        reader = csv.reader(f)

        fieldnames = []
        rows = []
        for i, row in enumerate(reader):
            if i >= general_config.CSV_ROWS:
                break

            if i == 0:
                for field in FORMAT_FIELDS:
                    if re.search(r'\b{}\b'.format(field), row[0]):
                        fieldnames.append(field)
            else:
                if len(row) == len(fieldnames):
                    rows.append([x.strip() for x in row])

    return fieldnames, rows
