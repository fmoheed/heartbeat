import string
import random


# Populate the auth DB with example entries
def build_sample_db(db, app, role, hash_password, data_store):
    db.drop_all()
    db.create_all()

    with app.app_context():
        user_role = role(name='User', description='General user - access to dashboard, Pulse search, and Pulse Processes')
        super_user_role = role(name='SuperUser', description='Admin user - access to dashboard, Pulse search, pulse processes, and User/Role dashboards')
        db.session.add(user_role)
        db.session.add(super_user_role)
        db.session.commit()

        # CREATE ADMIN USER --> Change later
        data_store.create_user(
            first_name='Admin',
            email='admin',
            roles=[user_role, super_user_role],
            password=hash_password('admin')
        )

        first_names = [
            'Harry', 'Amelia', 'Oliver', 'Jack', 'Isabella', 'Charlie', 'Sophie', 'Mia',
            'Jacob', 'Thomas', 'Emily', 'Lily', 'Ava', 'Isla', 'Alfie', 'Olivia', 'Jessica',
            'Riley', 'William', 'James', 'Geoffrey', 'Lisa', 'Benjamin', 'Stacey', 'Lucy'
        ]
        last_names = [
            'Brown', 'Smith', 'Patel', 'Jones', 'Williams', 'Johnson', 'Taylor', 'Thomas',
            'Roberts', 'Khan', 'Lewis', 'Jackson', 'Clarke', 'James', 'Phillips', 'Wilson',
            'Ali', 'Mason', 'Mitchell', 'Rose', 'Davis', 'Davies', 'Rodriguez', 'Cox', 'Alexander'
        ]

        for i in range(len(first_names)):
            tmp_email = first_names[i].lower() + "." + last_names[i].lower() + "@example.com"
            tmp_pass = ''.join(random.choice(string.ascii_lowercase + string.digits) for i in range(10))
            data_store.create_user(
                first_name=first_names[i],
                last_name=last_names[i],
                email=tmp_email,
                password=hash_password(tmp_pass),
                roles=[user_role, ]
            )
        db.session.commit()
    return
