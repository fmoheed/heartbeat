import os
import time
import csv

from flask import Flask, url_for, redirect, request, abort, flash, Response
from flask_sqlalchemy import SQLAlchemy
from flask_security import Security, SQLAlchemyUserDatastore, \
	UserMixin, RoleMixin, current_user
from flask_security.utils import hash_password
import flask_admin
from flask_admin import helpers as admin_helpers, BaseView, expose
from flask_admin.contrib import sqla

from concurrent.futures import ProcessPoolExecutor as Pool
import utils.core as core
import utils.test as test
from configs import general_config

# Create Flask application
application = Flask(__name__, static_url_path='/static')
application.config.from_pyfile('configs/application_config.py')
db = SQLAlchemy(application)


# Define DB Models
roles_users = db.Table(
	'roles_users',
	db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
	db.Column('role_id', db.Integer(), db.ForeignKey('role.id'))
)


class User(db.Model, UserMixin):
	id = db.Column(db.Integer, primary_key=True)
	first_name = db.Column(db.String(255))
	last_name = db.Column(db.String(255))
	email = db.Column(db.String(255), unique=True)
	password = db.Column(db.String(255))

	# EDIT LATER
	active = db.Column(db.Boolean())
	confirmed_at = db.Column(db.DateTime())
	# END EDIT LATER

	roles = db.relationship('Role', secondary=roles_users,
							backref=db.backref('user', lazy='dynamic'))

	def __str__(self):
		return self.email


class Role(db.Model, RoleMixin):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(80), unique=True)
	description = db.Column(db.String(255))

	def __str__(self):
		return self.name


class PulseProcesses(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	user_email = db.Column(db.String(255), db.ForeignKey('user.email'), nullable=False)
	process_status = db.Column(db.String(255))
	process_name = db.Column(db.String(255))
	process_command = db.Column(db.String(255))

	user = db.relationship('User', backref=db.backref('user', lazy='dynamic'))

	def __str__(self):
		return self.process_name


# Setup Flask-Security
data_store = SQLAlchemyUserDatastore(db, User, Role)
security = Security(application, data_store)


# Create customized model view class
class MyModelView(sqla.ModelView):
	def is_accessible(self):
		if not current_user.is_active or not current_user.is_authenticated:
			return False

		if current_user.has_role('SuperUser'):
			return True

		return False

	def _handle_view(self, name, **kwargs):
		"""
		Override builtin _handle_view in order to redirect users when a view is not accessible.
		"""
		if not self.is_accessible():
			if current_user.is_authenticated:
				# Permission denied
				abort(403)
			else:
				# Redirect to login page
				return redirect(url_for('security.login', next=request.url))

	edit_modal = True
	create_modal = True
	can_export = True
	can_view_details = True
	details_modal = True


# User View
class UserView(MyModelView):
	column_editable_list = ['email', 'first_name', 'last_name']
	column_searchable_list = column_editable_list
	column_exclude_list = ['password', 'confirmed_at']

	column_details_exclude_list = column_exclude_list
	column_filters = column_editable_list

	can_create = False


# Pulse Search View
class PulseView(BaseView):
	def is_accessible(self):
		if not current_user.is_active or not current_user.is_authenticated:
			return False

		return True

	def _handle_view(self, name, **kwargs):
		"""
		Override builtin _handle_view in order to redirect users when a view is not accessible.
		"""
		if not self.is_accessible():
			if current_user.is_authenticated:
				# Permission denied
				abort(403)
			else:
				# Redirect to login page
				return redirect(url_for('security.login', next=request.url))

	@expose('/')
	def index(self):
		return self.render('pulse/pulse.html')

	@expose('/admin_home')
	def home(self):
		return redirect(url_for('admin.index'))


# Pulse Processes View
class PulseProcessesView(MyModelView):
	def is_accessible(self):
		if not current_user.is_active or not current_user.is_authenticated:
			return False

		return True

	can_delete = False
	can_create = False

	column_searchable_list = ['user_email', 'process_name', 'process_command', 'process_status']
	column_filters = column_searchable_list


# Pulse CSV File Structure View
class PulseDataView(BaseView):
	def is_accessible(self):
		if not current_user.is_active or not current_user.is_authenticated:
			return False

		return True

	def _handle_view(self, name, **kwargs):
		"""
		Override builtin _handle_view in order to redirect users when a view is not accessible.
		"""
		if not self.is_accessible():
			if current_user.is_authenticated:
				# Permission denied
				abort(403)
			else:
				# Redirect to login page
				return redirect(url_for('security.login', next=request.url))

	@expose('/')
	def index(self):
		return self.render('pulse/pulse_data.html')

	@expose('/admin_home')
	def home(self):
		return redirect(url_for('admin.index'))

	@expose('/<filename_user>')
	def csv_file(self, filename_user):
		file_path = filename_user.split('|')[0]
		user = filename_user.split('|')[1]
		return redirect(url_for('pulsefileview.index', filename=file_path, user=user))


# Pulse CSV File View
class PulseFileView(BaseView):
	def is_accessible(self):
		if not current_user.is_active or not current_user.is_authenticated:
			return False

		return True

	def _handle_view(self, name, **kwargs):
		"""
		Override builtin _handle_view in order to redirect users when a view is not accessible.
		"""
		if not self.is_accessible():
			if current_user.is_authenticated:
				# Permission denied
				abort(403)
			else:
				# Redirect to login page
				return redirect(url_for('security.login', next=request.url))

	def is_visible(self):
		return False

	@expose('/', methods=['GET'])
	def index(self):
		filename = request.args.get('filename')
		user = request.args.get('user')
		fieldnames, rows = core.parse_csv(user, filename)
		return self.render('pulse/pulse_file.html', user=user, filename=filename, fieldnames=fieldnames, rows=rows)


# Flask views
@application.route('/')
@application.route('/index')
def index():
	return redirect(url_for('admin.index'))


@application.route('/pulse_form', methods=['GET', 'POST'])
def pulse_form():
	form_data = request.form

	time_string = time.strftime('%Y%m%d_%H%M%S')

	# Generate Command
	email_prefix = current_user.email.split('@')[0]
	cmd = core.gen_command(form_data, email_prefix, time_string)
	cmd = ' '.join(cmd)

	print(cmd)

	# Create searches subdirectory for user if it does not exist
	core.create_searches_subdirectory(email_prefix)

	# if core.TEST_FLAG:
	#     print(cmd)
	#     cmd = 'timeout 10 & echo foo'

	# Create and run child process
	val, error = core.check_command_ouput(cmd)

	if val:
		# Update PulseProcesses DB with RUNNING process entry
		process_name = core.create_searches_file_name(email_prefix, time_string)
		core.update_pulse_processes(db, PulseProcesses, current_user.email, process_name, cmd)

		# Add subprocess to pool
		pool = Pool(max_workers=10)
		pool.submit(core.execute_child_process(cmd))
	else:
		flash('Command Failed! Please try again.')

		# Remove empty CSV file
		filepath = cmd.split('>')[-1].strip()
		os.remove(filepath)

		return redirect(url_for('pulseview.index'))

	return redirect(url_for('pulseprocesses.index_view'))


@application.route('/export_csv', methods=['POST'])
def export_csv():
		export_data = request.form

		print(export_data['export_csv'])

		output = ''
		path = general_config.SEARCH_SUBDIRECTORY_PATH.format(export_data['export_csv'])[:-1]
		with open(path, 'r') as f:
			reader = csv.reader(f)

			for row in reader:
				output += ','.join(row) + '\n'

		return Response(
	        output,
	        mimetype="text/csv",
	        headers={"Content-disposition":
	                 "attachment; filename={}".format(export_data['export_csv'])})


# Create admin instance
admin = flask_admin.Admin(
	application,
	'Heartbeat',
	base_template='profile.html',
	template_mode='bootstrap3',
)

# Add model views
admin.add_view(MyModelView(Role, db.session, menu_icon_type='fa', menu_icon_value='fa-server', name='Roles'))
admin.add_view(UserView(User, db.session, menu_icon_type='fa', menu_icon_value='fa-users', name='Users'))
admin.add_view(PulseView(menu_icon_type='fa', menu_icon_value='fa-search', name='Pulse'))
admin.add_view(PulseProcessesView(PulseProcesses, db.session, menu_icon_type='fa', menu_icon_value='fa-database',
								  name='Pulse Processes'))
admin.add_view(PulseDataView(menu_icon_type='fa', menu_icon_value='fa-desktop', name='Pulse Data'))
admin.add_view(PulseFileView(name='Pulse File'))


# Context processor for app
@application.context_processor
def pulse_data_processor():
	# Check if /log/searches/ directory exists
	def dir_exists():
		return os.path.exists(general_config.SEARCH_DIRECTORY_PATH)

	# Get user subdirectories
	def get_users():
		path = general_config.SEARCH_DIRECTORY_PATH
		return [name for name in os.listdir(path)
				if os.path.isdir(os.path.join(path, name))]

	# Get search files in subdirectory
	def get_search_files(user):
		path = general_config.SEARCH_SUBDIRECTORY_PATH.format(user)
		return [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

	return dict(dir_exists=dir_exists, get_users=get_users, get_search_files=get_search_files)


# Context processor for combining Flask Security with Admin Views
@security.context_processor
def security_context_processor():
	return dict(
		admin_base_template=admin.base_template,
		admin_view=admin.index_view,
		h=admin_helpers,
		get_url=url_for
	)


# Create database tables, check if test flag active
if core.TEST_FLAG:
	# Run test.build_sample_db
	app_dir = os.path.realpath(os.path.dirname(__file__))
	database_path = os.path.join(app_dir, application.config['DATABASE_FILE'])
	if not os.path.exists(database_path):
		test.build_sample_db(db, application, Role, hash_password, data_store)
else:
	# Run the core.py populate_db function
	if not core.db_exists():
		# Creates DB with admin user
		core.create_db(db, application, Role, hash_password, data_store)

# Create searches directory
core.create_searches_directory()

if __name__ == 'main':
	# Start app
	application.run(debug=True, threaded=True)
